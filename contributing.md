# MLOpsHW1

## Using Development Tools

Project used: pdm, pre-commit, ruff, mypy.

Firstly, install pdm is `pip install --user pdm`. Then install the dependencies by creating a virtual environment, `python -m pdm install` and activate it, for example for cmd `venv\Scripts\activate.bat`

You may need to run `pdm run install_pre_commit` for pre-commit to work.

The following commands are also available
- `pdm lint` - style check
- `pdm format` - Code formatting.
- `pdm check_types` - checks the correct use of type annotations.
- `pdm run` - Run the application
- `pdm start` - sequential execution of `lint`, `format`, `check_types` and `run`

The linter and code builder will be used automatically when you try to commit.

## Repository Introduction Methodology

Following the GitHub thread

1. **Create a branch** in your repository from the "main" branch with a short descriptive name. Create branches for each separate, unrelated set of changes.

2. **Make any changes**, committing and pushing them to your branch, giving each commit a descriptive message. **Follow [Conventional Commits](https://www.conventionalcommits.org/).** Ideally, each commit contains an isolated full change. Keep making, committing, and making changes to your branch until you're ready to ask for feedback. **Use "pre-commint"** to check your changes against styleguide and other checks. **Pay attention to the progress of the cicd conveyor**, it will also show whether your changes meet the necessary requirements.

3. **Create a pull request.** Create a pull request to ask collaborators for feedback on your changes. If you need feedback or advice before you make changes, you can mark your pull request as a draft. When creating a pull request, provide a brief description of the changes and what problem they solve. You can also manually @mention or request feedback from specific people or teams. **Pay attention to the progress of the cicd conveyor**, it will also show whether your changes meet the necessary requirements.

4. **Comments on the address review.** Reviewers are encouraged to post questions, comments, and suggestions. Reviewers can comment on the entire pull request or add comments to specific lines or files.

5. **Merge your pull request.** Once your pull request is approved, merge it. This will automatically merge your branch and your changes will appear in the default branch. **Pay attention to the progress of the cicd pipeline**, it will show whether your changes meet the necessary requirements and how the process of building and deploying the project proceeds.

6. **Delete your thread.** After merging the pull request, delete your branch.

## Project structure

* `/.mypy_cache`, `/.pdm-build`, `/.ruff_cache`, `/.venv` - not stored in the repository, temporary files (not edited manually)
* `/data/external/<source_name>/<dataset_name>/` - not stored in the repository, dataset with the name `<dataset_name>` obtained from external resource `<source_name>`
* `/data/interim/<dataset_name>/` - not stored in the repository, dataset with the name `<dataset_name>` obtained during data preprocessing
* `/data/processed/<dataset_name>/` - not stored in the repository, processed dataset with the name `<dataset_name>`
* `/data/raw/<dataset_name>/` - not stored in the repository, raw internal dataset with the name `<dataset_name>`
* `/dist/` - not in the repository, storage location for compiled python packages
* `/docs/` - storage location for `.md` files for documentation and their resources (commit uses `docs`)
* `/models/<model_name>/` - not stored in the repository, model with the name `<model_name>`
* `/notebooks/` - folder to store jupyter notebooks for research, its content is used to create documentation (commit uses `chore(notebooks)`)
* `/public/` - not in the repository, compiled documentation, used in gitlab-pages
* `/references/` - references to external research (commit uses `chore(references)`)
* `/reports/` - storage location for finalized reports (commit uses `chore(reports)`)
* `/scripts/` - storage location for auxiliary scripts for repository management (commit uses `chore(scripts)`)
* `/src/` - source code of packages, no new files or folders should be added
* `/src/mlopshw1/` - source code to be built into a library for production, its content is used to create documentation (commit uses various commit types)
* `/tests/` - tests for source code (commit uses `test`)
* `/.dockerignore`, `/.gitignore` - exclusion files, should be identical (commit uses `chore(ignore)`)
* `/.gitlab-ci` - description of CI/CD gitlab pipeline (commit uses `ci`)
* `/.pre-commit-config.yaml` - pre-commit configuration (commit uses `chore(pre-commit)`)
* `/all_requirements.dockerfile` - dockerfile with all project dependencies, used in CI (commit uses `build`)
* `/contributing.md` - this file with project introduction description, its content is used to create documentation (commit uses `docs`)
* `/LICENSE.txt` - license, its content is used to create documentation (commit uses `feat(license)!`)
* `/mkdocs.yml` - mkdocs configuration (commit uses `docs`)
* `/pdm.lock` - project dependencies frozen (not edited manually)
* `/pyproject.toml` - project build settings, linters, dependencies, etc. (commit uses various commit types)
* `/README.md` - project description file, its content is used to create documentation (commit uses `docs`)
* `/requirements.dev.txt`, `requirements.txt` - dependencies in pip format (not edited manually)
