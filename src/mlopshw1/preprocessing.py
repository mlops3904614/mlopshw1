from typing import Any

import pandas as pd
import numpy as np


def cat2bools_(
    df: pd.DataFrame,
    from_values: int = 2,
    to_values: int = 5,
    delete_if_one_value: bool = True,
) -> list[str]:
    """_summary_

    Args:
        df (pd.DataFrame): _description_
        from_values (int, optional): _description_. Defaults to 2.
        to_values (int, optional): _description_. Defaults to 5.
        delete_if_one_value (bool, optional): _description_. Defaults to True.

    Returns:
        list[str]: _description_
    """
    deleted_columns = []
    for col in df.columns:
        value_counts = df[col].value_counts()
        if sum(list(value_counts)) != len(df):
            continue
        if delete_if_one_value and (len(value_counts.index) == 1):
            deleted_columns.append(col)
        elif from_values <= len(value_counts.index) < to_values:
            for i, j in value_counts.items():
                df[f"is_{col}_{i}"] = df[col] == i
            deleted_columns.append(col)
    df.drop(columns=deleted_columns, inplace=True)
    return deleted_columns


def cat2int_(
    df: pd.DataFrame, from_values: int = 2, to_values: int = 5
) -> dict[str, dict[Any, int]]:
    """_summary_

    Args:
        df (pd.DataFrame): _description_
        from_values (int, optional): _description_. Defaults to 2.
        to_values (int, optional): _description_. Defaults to 5.

    Returns:
        list[str]: _description_
    """
    dicts = {}

    for col in df.columns:
        value_counts = df[col].value_counts()
        if from_values <= len(value_counts.index) < to_values:
            dicts[f"{col}__to__index"] = {
                n: i for i, n in enumerate(value_counts.index)
            }
            dicts[f"index__to__{col}"] = {
                i: n for i, n in dicts[f"{col}__to__index"].items()
            }
            df[col] = df[col].apply(
                lambda x: dicts[f"{col}__to__index"].get(x)
            )
            df[col] = df[col].astype(np.float64)
    return dicts
