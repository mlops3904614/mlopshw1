import pandas as pd
import numpy as np

from matplotlib.figure import Figure


def load_data(path="", limit=1000):
    import pandas as pd

    df = pd.read_csv(path, nrows=int(limit), header=None)
    df.columns = ["class", "title", "text"]
    df = df.head(int(limit))
    return df


def preprocess(df: pd.DataFrame):
    import re

    import nltk

    class Preprocessor:
        def __init__(self):
            from nltk.corpus import stopwords
            from nltk.stem import WordNetLemmatizer

            nltk.download("stopwords")
            nltk.download("wordnet")

            self.stop_words = set(stopwords.words("english"))
            self.url_pattern = re.compile(
                r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*"
            )
            self.spec_chars_pattern = re.compile("[0-9 \-_]+")
            self.non_alpha_pattern = re.compile("[^a-z A-Z]+")
            self.lemmatizer = WordNetLemmatizer()

        def text_preprocessing(self, input_text: str) -> str:
            try:
                text = input_text.lower()
                text = self.url_pattern.sub("", text)
                text = self.spec_chars_pattern.sub(" ", text)
                text = self.non_alpha_pattern.sub(" ", text)
                text = " ".join(
                    w for w in text.split() if w not in self.stop_words
                )
                text = text.strip().split(" ")
                text = [self.lemmatizer.lemmatize(token) for token in text]
                return " ".join(text)
            except:  # noqa
                return ""

    columns = df.select_dtypes(include=["object"]).columns.tolist()
    preprocesser = Preprocessor()
    for col in columns:
        df[str(col) + "_preprocessed"] = df[col].apply(
            preprocesser.text_preprocessing
        )
    preprocessed_columns = [str(col) + "_preprocessed" for col in columns]
    df["_corpus"] = df[preprocessed_columns].apply(
        lambda row: "_".join(row.values.astype(str)), axis=1
    )
    df["split"] = np.random.choice(
        ["train", "val", "test"], size=len(df), p=[0.8, 0, 0.2]
    )
    return df


def train_vectorizer(df):
    from sklearn.feature_extraction.text import TfidfVectorizer

    df = df[df.split == "train"]
    vec = TfidfVectorizer()
    vec.fit(df["_corpus"].to_numpy())
    vec._stop_words_id = 0
    return vec


def vectorize(df, vec):
    array = vec.transform(df["_corpus"].to_numpy()).toarray()
    df["_vecs"] = [row.tolist() for row in array.astype(float)]
    return df


def train_model(df=None, model_type=None):
    import json
    import pickle
    import numpy as np
    import pathlib
    from sklearn.linear_model import LogisticRegression
    from sklearn.svm import SVC
    from clearml import PipelineController

    df_train = df[df.split == "train"]
    df_train["_vecs"] = df_train["_vecs"].apply(json.loads)
    df_train["_vecs"] = df_train["_vecs"].apply(np.array)
    if model_type == "sklearn_lr":
        model = LogisticRegression()
        model.fit(np.vstack(df_train["_vecs"]), df_train["class"])
    elif model_type == "sklearn_svc":
        model = SVC()
        model.fit(np.vstack(df_train["_vecs"]), df_train["class"])
    path = pathlib.Path(f"models/amazon_{model_type}/model.pkl")
    path.parent.mkdir(parents=True, exist_ok=True)
    pickle.dump(model, path.open("wb"))
    PipelineController.upload_model(f"amazon_{model_type}", path)
    return model


def test(df, model):
    import json
    import numpy as np
    from sklearn.metrics import classification_report
    from matplotlib import pyplot as plt
    from clearml import PipelineController

    def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
        from sklearn.metrics import ConfusionMatrixDisplay

        plt.ioff()
        fig, ax = plt.subplots(figsize=(5, 5))
        ConfusionMatrixDisplay.from_predictions(
            y_true, pred, ax=ax, colorbar=False
        )
        ax.xaxis.set_tick_params(rotation=90)
        _ = ax.set_title("Confusion Matrix")
        plt.tight_layout()
        return fig

    df = df[df.split == "test"]
    df["_vecs"] = df["_vecs"].apply(json.loads)
    df["_vecs"] = df["_vecs"].apply(np.array)
    predicts = model.predict(np.vstack(df["_vecs"]))
    fig = conf_matrix(df["class"].astype(float), predicts)
    met = classification_report(
        df["class"].astype(float), predicts, output_dict=True
    )
    logger = PipelineController.get_logger()
    logger.report_single_value("accuracy", met["accuracy"])
    logger.report_single_value("precision", met["weighted avg"]["precision"])
    logger.report_single_value("recall", met["weighted avg"]["recall"])
    logger.report_single_value("f1-score", met["weighted avg"]["f1-score"])
    logger.report_single_value("support", met["weighted avg"]["support"])
    logger.report_matplotlib_figure("conf_matrix", "val", figure=fig)
    return met


def main():
    from clearml import PipelineController

    pipe = PipelineController(
        project="amazon",
        name="Amazon pipeline",
        version="2.0",
        add_pipeline_tags=False,
    )
    pipe.set_default_execution_queue("default")

    pipe.add_parameter(name="path", default=r"data\raw\train.csv")

    pipe.add_parameter(
        name="model_type",
        default=r"sklearn_lr",
        param_type="Only 'sklearn_lr' and 'sklearn_svc' are implemented",
    )

    pipe.add_parameter(
        name="limit", default=1000, description="Dataset items limit"
    )

    pipe.add_function_step(
        name="load_data",
        function=load_data,
        function_kwargs=dict(
            path="${pipeline.path}", limit="${pipeline.limit}"
        ),
        function_return=["data_frame"],
        cache_executed_step=True,
        task_type="data_processing",
    )

    pipe.add_function_step(
        name="preprocess",
        parents=["load_data"],
        function=preprocess,
        function_kwargs=dict(df="${load_data.data_frame}"),
        function_return=["data_frame"],
        cache_executed_step=True,
        task_type="data_processing",
    )

    pipe.add_function_step(
        name="train_vectorizer",
        parents=["preprocess"],
        function=train_vectorizer,
        function_kwargs=dict(df="${preprocess.data_frame}"),
        function_return=["vec"],
        cache_executed_step=True,
        task_type="training",
    )

    pipe.add_function_step(
        name="vectorize",
        parents=["train_vectorizer", "preprocess"],
        function=vectorize,
        function_kwargs=dict(
            df="${preprocess.data_frame}", vec="${train_vectorizer.vec}"
        ),
        function_return=["data_frame"],
        cache_executed_step=True,
        task_type="data_processing",
    )

    pipe.add_function_step(
        name="train_model",
        parents=["vectorize"],
        function=train_model,
        function_kwargs=dict(
            df="${vectorize.data_frame}", model_type="${pipeline.model_type}"
        ),
        function_return=["model"],
        cache_executed_step=True,
        task_type="training",
    )

    pipe.add_function_step(
        name="test_model",
        parents=["train_model", "vectorize"],
        function=test,
        function_kwargs=dict(
            df="${vectorize.data_frame}", model="${train_model.model}"
        ),
        function_return=["metrics"],
        cache_executed_step=True,
        task_type="testing",
    )

    pipe.start_locally(True)

    print("pipeline completed")


if __name__ == "__main__":
    main()
