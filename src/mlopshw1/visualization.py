import math


import plotly.graph_objects as go
import matplotlib.pyplot as plt


def show_nan_pies(df, COLUMNS=3):
    df_temp = df.isna().sum() * 100 / len(df)
    df_temp = df_temp[df_temp != 0]
    data = list(df_temp)
    labels = list(df_temp.index)
    rows = math.ceil(len(data) / COLUMNS)
    fig, ax = plt.subplots(rows, COLUMNS, figsize=(COLUMNS * 4, rows * 3))
    for i, (d, lab) in enumerate(zip(data, labels)):
        x, y = i // COLUMNS, i % COLUMNS
        ax[x, y].pie(
            [d, 100 - d],
            labels=[f"Null: {d:.3}%", f"No Null: {100-d:.3}%"],
            autopct="%1.1f%%",
        )
        ax[x, y].set_title(lab)
    return fig


def print_table(array, row_index=None, col_index=None, title=""):
    print(title)
    print("", *row_index, sep="\t")
    for i in col_index:
        print(i, "\t|", end="")
        for j in row_index:
            print(round(array[i, j], 3), end="\t")
        print()


def show_pair_scatter(df, pair_cols, COLUMNS=3, counts=1000):
    rows = math.ceil(len(pair_cols[0]) / COLUMNS)
    fig, ax = plt.subplots(rows, COLUMNS, figsize=(COLUMNS * 5, rows * 5))
    for i in range(len(pair_cols[0])):
        col_1 = pair_cols[0][i]
        col_2 = pair_cols[1][i]
        index_1 = list(df[df.columns[col_1]].sample(counts).index)
        index_2 = list(df[df.columns[col_2]].sample(counts).index)
        val_1 = list(df[df.columns[col_1]].sample(counts))
        val_2 = list(df[df.columns[col_2]].sample(counts))
        x, y = i // COLUMNS, i % COLUMNS
        ax[x, y].scatter(index_1, val_1)
        ax[x, y].scatter(index_2, val_2)
        ax[x, y].set_title(f"{df.columns[col_1]} и {df.columns[col_2]}")
    return fig


def show_pair_distribution(df, pair_cols, COLUMNS=3, counts=1000):
    rows = math.ceil(len(pair_cols[0]) / COLUMNS)
    fig, ax = plt.subplots(rows, COLUMNS, figsize=(COLUMNS * 5, rows * 5))
    for i in range(len(pair_cols[0])):
        col_1 = pair_cols[0][i]
        col_2 = pair_cols[1][i]
        val_1 = list(df[df.columns[col_1]].sample(counts))
        val_2 = list(df[df.columns[col_2]].sample(counts))
        x, y = i // COLUMNS, i % COLUMNS
        ax[x, y].hist(val_1)
        ax[x, y].hist(val_2)
        ax[x, y].set_title(f"{df.columns[col_1]} и {df.columns[col_2]}")
    return fig


def show_map(df, lat="latitude", lon="longitude"):
    scatter_mapbox = go.Scattermapbox(lat=df["latitude"], lon=df["longitude"])
    layout = go.Layout(
        mapbox=dict(
            style="carto-positron",
            zoom=9,
            center={
                "lat": df[lat].median(),
                "lon": df[lon].median(),
            },
        )
    )
    return go.Figure(data=[scatter_mapbox], layout=layout)
