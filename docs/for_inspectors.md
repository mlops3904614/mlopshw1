# Задание и решения

## 1. Задание по репозиторию (курс MLOps и production в DS исследованиях 3.0)

Цель этого домашнего задания подготовить репозиторий для работы в нем: настроить необходимые линтеры и форматеры, описать конфиг pre-commit и сформировать contributing.md где описано, что и как нужно делать в репозитории для внесения изменений.

Для этого нужно выполнить следующие шаги:

1. Опубликовать репозиторий на gitlab (или аналоге, но курс ориентирован на gitlab).

2. Выбрать линтеры и форматеры. Можно выбирать те, которые озвучивались в материалах курса, или какие-то свои, которые вам привычны.

3. Зафиксировать необходимые зависимости для линтеров

4. Настроить pre-commit в репозитории

5. Провести настройки линтеров и форматеров в pyproject.toml, прописать основные параметры инструментов

6. Зафиксировать в contributing.md, как пользоваться линтерами в вашем проекте

7. * добавить в readme.md методологию ведения вашего репозитория

Оценка домашнего задания будет проходить по наличию зависимостей линтеров и форматеров в репозитории, файлов их конфигурации, настроенного pre-commit и описания как пользоваться всеми инструментами.

## Решение 1 задания

1. Репозиторий: https://gitlab.com/mlops3904614/mlopshw1

2. Выбран ruff в качестве линтера и форматера из-за высокой производительности, а также mypy для проверки аннотации типов из-за популярности решения

3. Зависимости зафиксированы с помощью пакетного менеджера: pdm [pyproject.toml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/pyproject.toml), [lock-файл](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/pdm.lock), [requirements.txt с зависимостями разработки](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/requirements.dev.txt)

4. Конфиг [.pre-commit-config.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/.pre-commit-config.yaml) было добавлено проверка и форматирование кода с помощью ruff, а также проверка на типы mypy

5. [pyproject.toml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/pyproject.toml) Поскольку styleguide команды не сформирован, использованы настройки по умолчанию, но для демонстрации умения настраивать было добавлено правило на максимальную длину строки, для mypy активированы ещё два предупреждения

6. [contributing.md](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/contributing.md#using-development-tools) написано все необходимое для настройки рабочего окружения и запуску инструментов

7. Указания по ведению репозитория были в файле [contributing.md](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/contributing.md#using-development-tools), методология была основана на github flow, поскольку она лучше всего подходит для непрерывной разработки

## 2. Задание по docker (курс MLOps и production в DS исследованиях 3.0)

Цель этого домашнего задания подготовить себе docker образ для работы с ним. В дальнейшем вы можете использовать его как рабочее место для экспериментов, использовать для CI-CD и поставки зависимостей. Нужно зафиксировать зависимости с помощью одного из пакетных менеджеров (попробуйте что-то новое, не используйте pip), разделите зависимости по группам/окружениям (например dev и prod, в dev можно поместить pre-commit и ruff) выбрать нужный базовый образ и прописать в docker file нужные штаги по настройки окружения и зависимостей.

Для этого нужно выполнить следующие шаги:

1. Зафиксировать зависимости с помощью одного из пакетных менеджеров

2. Составить Dockerfile с построением вашего окружения

3. Прописать команды для его сборки и запуска в readme проекта.

Оценка домашнего задания будет проходить по наличию зависимостей сформированных с помощью пакетных менеджеров, dockerfile и команды к нему.

## Решение 2 задания

1. Был использован pdm [pyproject.toml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/pyproject.toml), [lock-файл](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/pdm.lock), [requirements.txt с зависимостями разработки](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/requirements.dev.txt), в [cicd](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/.gitlab-ci.ym) конвейер была прописана команда по проверки корректности requirements файлов

2. [all_requirements.dockerfile](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/all_requirements.dockerfile) - создает образ со всеми зависимостями, можно использовать для сборки пакета

3. [readme.md](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/readme.md) - добавлена команда для создания docker-образа и его запуска

## 3. Задание по CI-CD (курс MLOps и production в DS исследованиях 3.0)

Цель этого домашнего задания настроить CI-CD пайплайн и опубликовать на gitlab pages документацию проекта и исследований. пайплан должен включать в себя:

1. DinD – сборку вашего докер образа из предыдущего дз, стоит также опубликовать образ в вашем gitlab docker registry.

2. Линтеринг кода с использованием выбранных линтеров и форматеров

3. Сборка и публикация вашего проекта в виде пакета в gitlab pypi registry.

4. Сборка исследования из quarto/jupyter, документации в html (возможно надо будет скачать данные с помощью kaggle-cli)

5. Публикация на gitlab pages

Помните, что в ci-cd различные credentials надо задавать как секреты – masked variables.

Проведите разведочный анализ на датасет ny-2015-street-tree-census-tree-data. В исследование стоит отобразить следующее:

1. Показать при помощи таблиц превью данных.

2. Продемонстрировать при помощи таблиц или графиков объем пропущенных значений в данных. Например, можно для столбцов, где есть пропуски, построить pie chart с указанием процента пропусков (есть в plotly).

3. Построить диаграммы попарного распределения признаков.

4. Рассчитать и показать матрицу попарных корреляций между вещественными признаками.

5. Отобразить географическое представление деревьев из датасета. В датасете есть поля: latitude и longitude, и, используя библиотеки, которые позволяют отображать положение объектов на карте при помощи широты и долготы, необходимо предоставить в отчете карту расположения деревьев из данных.

Не забудьте зафиксировать ваши наблюдения и выводы полученные в ходе исследования, что интересного вы заметили и как вы это интерпретировали.

## Решение 3 задания

По CI:

1. [.gitlab-ci.yml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/.gitlab-ci.yml?ref_type=heads) и [all_requirements.dockerfile](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/all_requirements.dockerfile?ref_type=heads)

2. [.gitlab-ci.yml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/.gitlab-ci.yml?ref_type=heads)

3. [.gitlab-ci.yml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/.gitlab-ci.yml?ref_type=heads)

4. Использон mkdocs пришлось создать свой плагин для автоматической вставки ноутбуков в документацию [.gitlab-ci.yml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/.gitlab-ci.yml?ref_type=heads), [mkdocs.yml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/mkdocs.yml?ref_type=heads), [gen_ref_pages_code.py](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/scripts/gen_ref_pages_code.py?ref_type=heads), [gen_ref_pages_md.py](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/scripts/gen_ref_pages_md.py?ref_type=heads), [gen_ref_pages_notebooks.py](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/scripts/gen_ref_pages_notebooks.py?ref_type=heads)

5. [Сайт](https://mlopshw1-mlops3904614-6754ee4a48c94b5a8bd893315442119dbca88f11f.gitlab.io/) доступен

По EDA:

1. [На Pages](https://mlopshw1-mlops3904614-6754ee4a48c94b5a8bd893315442119dbca88f11f.gitlab.io/notebooks/EDA/), [Файл](https://gitlab.com/mlops3904614/mlopshw1/-/blob/main/notebooks/EDA.ipynb?ref_type=heads)

2. См 1.

3. См 1.

4. См 1.

5. См 1.


## 4. Задание по блоку Snakemake (курс MLOps и production в DS исследованиях 3.0)

1. Реализовать пайплайн обработки данных при помощи snakemake, минимум 3 правила - чтение данных, препроцессинг данных, обучение модели. (4 балла)

2. Добавить как минимум ещё 1 вариант препроцессинга и ещё 1 вариант обучения модели, настроив правила таким образом, чтобы в результате работы пайплана получились минимум 4 артефакта - комбинация двух моделей с двумя разными препроцессингами. (2 балла)

3. Обобщить хотя бы одно правило, используя named wildcards. (1 балл)

4. Воспользоваться хотя бы в одном правиле вспомогательной функцией expand. (1 балл)

5. Добавить readme или отчёт в формате md/qmd, с описанием шагов вашего пайплайна и отображением dag, полученного при помощи внутренних функций snakemake. (1 балл)

6. Использовать ограничение правил по ресурсам, например, параметр threads до половины числа ядер машины. (1 балл)

## Решение 4 задания

Сделанно в ветке [dev-use-snakemake](https://gitlab.com/mlops3904614/mlopshw1/-/tree/dev-use-snakemake?ref_type=heads)

- Все требования выполнены: [Snakemake](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-snakemake/workflow/Snakefile?ref_type=heads)
- [Readme](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-snakemake/README.md?ref_type=heads)

## 5. Задание по блоку Hydra (курс MLOps и production в DS исследованиях 3.0)

1. Создать как минимум две группы конфигов (для параметров препроцессинга и самой модели), добавить в них структурированные ИЛИ yaml-конфиги (4 балла)

2. Интегрировать чтение конфигураций через Compose-API в код разведочного анализа или пайплайнов (например, в скрипт Snakemake), использовать несколько произвольных параметров из прочитанных конфигураций в этом коде (2 балла)

3. Использовать instantiate для инициализации модели внутри вспомогательных python-модулей (или скрипта Snakemake) (2 балла)

4. Добавить (любым удобным способом) в переменные окружения число ядер процессора на своей машине. В одном из конфигов воспользоваться reslover'ом "oc.env" для чтения числа ядер. Использовать это значение в коде обучения или инициализации моделей, ограниив потребление cpu половиной доступных ядер (как вариант - использовать в ограничениях правил Snakemake) (2 балла)

## Решение 5 задания

Сделанно в ветке [dev-use-hydra](https://gitlab.com/mlops3904614/mlopshw1/-/tree/dev-use-hydra?ref_type=heads)

1. Созданны группы конфигураци: data ([titanic.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/data/titanic.yaml?ref_type=heads)), model ([tree.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/model/tree.yaml?ref_type=heads), [knn.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/model/knn.yaml?ref_type=heads)), preprocessing ([tree.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/preprocessing/tree.yaml?ref_type=heads), [knn.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/preprocessing/knn.yaml?ref_type=heads)), secrets ([secrets.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/preprocessing/secrets.yaml?ref_type=heads)), также есть [config.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/preprocessing/config.yaml?ref_type=heads)
   
2. [Train_Titanic.ipynb](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/notebooks/Train_Titanic.ipynb?ref_type=heads) - сделанно отдельно, так как Snakemake уже вдругой ветке и не хотелось портить разведочный анализ
   
3. Использованно внутри [Train_Titanic.ipynb](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/notebooks/Train_Titanic.ipynb?ref_type=heads) и [train_model.py](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/mlopshw1/train_model.py?ref_type=heads)

4. Добавнно в начале скриптов [Train_Titanic.ipynb](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/notebooks/Train_Titanic.ipynb?ref_type=heads) и [train_model.py](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/mlopshw1/train_model.py?ref_type=heads), используется в [tree.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/model/tree.yaml?ref_type=heads), [knn.yaml](https://gitlab.com/mlops3904614/mlopshw1/-/blob/dev-use-hydra/src/conf/model/knn.yaml?ref_type=heads)
